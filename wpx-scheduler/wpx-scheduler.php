<?php
/*
Plugin Name: WPX Scheduler
Plugin URI: http://7learn.com
Description: a plugin for schedule tasks
Author: Kaivan Alimohammadi
Version: 1.0.0
Author URI: http://7learn.com
*/

register_activation_hook( __FILE__, 'activate' );
register_deactivation_hook(__FILE__,'deactivation');

function activate() {
	if ( ! wp_next_scheduled( 'wpx_optimize_db' ) ) {
		wp_schedule_event( time(), 'daily', 'wpx_optimize_db' );
	}
	//wp_schedule_single_event()
}

function wpx_send_optimize_email_to_admin() {

}

add_action( 'wpx_optimize_db', 'wpx_send_optimize_email_to_admin' );

function deactivation(){
	wp_clear_scheduled_hook('wpx_optimize_db');
}