<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>پنل کاربری</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-rtl/3.4.0/css/bootstrap-rtl.min.css">
	<style>
		body{
			padding: 50px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="row">
		<div class="col-xs-12 col-md-3">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">منوی کاربری</h3>
				</div>
				<div class="panel-body">
					<ul class="list-group">
						<li class="list-group-item">
							<a href="/panel/profile">پروفایل کاربری</a>
						</li>
                        <li class="list-group-item">
                            <a href="/panel/orders">سفارش ها</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-9">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title"><?php echo  isset($params['panel_title']) ? $params['panel_title']: ''; ?></h3>
				</div>
				<div class="panel-body">
					<form class="form-horizontal" method="post">

						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">نام نمایشی :</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="display_name"
								       name="display_name"
								value="<?php echo isset($currentUser) ? $currentUser->display_name : ''; ?>"
								>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">ایمیل  :</label>
							<div class="col-sm-10">
								<input type="email" class="form-control" id="user_email"
								       name="user_email"
								value="<?php echo isset($currentUser) ? $currentUser->user_email : ''; ?>"
								>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">شماره همراه  :</label>
							<div class="col-sm-10">
								<input type="text" class="form-control" id="user_mobile"
								       name="user_mobile"
								       value="<?php echo isset($userInfo) ? $userInfo->user_mobile : ''; ?>"
								>
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">کلمه عبور  :</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" name="user_password" id="user_password">
							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-2 control-label">تکرار کلمه عبور  :</label>
							<div class="col-sm-10">
								<input type="password" class="form-control" id="user_password_confirm"
								name="user_password_confirm">
							</div>
						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" name="submit" class="btn btn-default">ذخیره اطلاعات</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>