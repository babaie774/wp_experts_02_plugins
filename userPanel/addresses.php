<?php
return [
	'/panel'  => 'Application\\Handler\\PanelHandler@index',
	'/panel/profile'  => 'Application\\Handler\\ProfileHandler@index',
	'/panel/orders'  => 'Application\\Handler\\OrderHandler@index',
];