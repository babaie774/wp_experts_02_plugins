<?php
/*
Plugin Name: WPX ACL
Plugin URI: http://7learn.com
Description: a plugin for manage ACL
Author: Kaivan Alimohammadi
Version: 1.0.0
Author URI: http://7learn.com
*/

function wpx_acl_activation() {
	add_role( 'marketer', 'Marketer', [
		'read'                   => true,
		'manage_instagram'       => true,
		'manage_telegram'        => true,
		'view_instagram_options' => true
	] );
}

register_activation_hook( __FILE__, 'wpx_acl_activation' );

function wpx_acl_handler() {

	if ( isset( $_POST['save_acl'] ) ) {
		$user_id = $_POST['user_id'];
		$caps    = $_POST['caps'];
		if ( $caps ) {
			foreach ( $caps as $cap ) {
				Application\Service\Acl\AclService::addCap( $user_id, $cap );

			}
		}
	}

	$subscribers = $user_query = new WP_User_Query( array( 'role' => 'Subscriber' ) );
	$caps        = [
		'edit_product'   => 'ویرایش محصول',
		'delete_product' => 'حذف محصول',
		'add_product'    => 'اضافه کردن محصول',
	];
	include "view.php";

}

add_action( 'admin_menu', function () {
	add_menu_page( 'مدیریت دسترسی ها',
		'مدیریت دسترسی ها',
		'manage_options',
		'wpx_acl',
		'wpx_acl_handler'

	);
} );