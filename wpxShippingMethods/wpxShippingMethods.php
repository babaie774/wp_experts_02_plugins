<?php
/*
Plugin Name: wpx Shipping Methods
Plugin URI: http://7learn.com
Description: a plugin for replace words
Author: Kaivan Alimohammadi
Version: 1.0.0
Author URI: http://7learn.com
*/
add_action('after_setup_theme',function (){
	class WPX_Shipping extends WPX_Section_Contract {

		public static $title = 'روش های ارسال';

		public static $tab = 'shipping';

		public function __construct() {
			parent::__construct();
			$this->viewFile = plugin_dir_path(__FILE__).'shippings.php';
		}

		public function render() {
			$wpx_options = $this->wpx_options;
			include $this->viewFile;
		}

		public function save() {
			// TODO: Implement save() method.
		}
	}

	add_filter('wpx_section_handlers',function ($handlers){
		$handlers[]='WPX_Shipping';
		return $handlers;
	});
});